#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

int main(int argc, char *argv[]) {
	// Open file.
	if (argc != 2) {
		printf("Usage: ./testbench logfile.log");
		return 1;
	}
	char *filename = argv[1];
	FILE *file = fopen(filename, "r");
	// Read in the file and tokenize into lines.
	fseek(file, 0L, SEEK_END);
	int filesize = ftell(file);
	rewind(file);
	char *filedata = malloc(filesize + 1);
	fread(filedata, 1, filesize, file);
	char *strtok_r_state;
	char *line = strtok_r(filedata, "\n", &strtok_r_state);
	// Initialize state for calculation of the ideal values.
	int pixel_x_ideal_int = 0;
	int pixel_y_ideal_int = 0;
	float PIXEL_WIDTH = 1000.0;
	float PIXEL_HEIGHT = 1000.0;
	printf("Actual\tIdeal\n");
	while (line != NULL) {
		// Read in the program output and convert it to floats.
		char *pixel_x_raw = strtok(line, ",");
		char *pixel_y_raw = strtok(NULL, ",");
		int pixel_x_int = (int) strtol(pixel_x_raw, NULL, 16);
		int pixel_y_int = (int) strtol(pixel_y_raw, NULL, 16);
		float pixel_x, pixel_y;
		memcpy(&pixel_x, &pixel_x_int, sizeof(float));
		memcpy(&pixel_y, &pixel_y_int, sizeof(float));
		pixel_x = roundf(pixel_x * 100) / 100;
		pixel_y = roundf(pixel_y * 100) / 100;
		line = strtok_r(NULL, "\n", &strtok_r_state);
		// Create ideal program output.
		float pixel_x_ideal_float = -2.5 + (((float) pixel_x_ideal_int) / PIXEL_WIDTH) * 3.5;
		float pixel_y_ideal_float = -1.0 + (((float) pixel_y_ideal_int) / PIXEL_HEIGHT) * 2.0;
		pixel_x_ideal_float = roundf(pixel_x_ideal_float * 100) / 100;
		pixel_y_ideal_float = roundf(pixel_y_ideal_float * 100) / 100;
		// Write comparison info.
		if (pixel_x == pixel_x_ideal_float && pixel_y == pixel_y_ideal_float) {
			printf("\033[1;32m");
		}
		else {
			printf("\033[1;31m");
		}
		printf("%f,%f\t", pixel_x, pixel_y);
		printf("%f,%f\t", pixel_x_ideal_float, pixel_y_ideal_float);
		printf("\033[0m\n");
		// Update state for calculation of the ideal values.
		if (pixel_x_ideal_int < (int) PIXEL_WIDTH) {
			pixel_x_ideal_int++;
		}
		else {
			pixel_x_ideal_int = 0;
			pixel_y_ideal_int++;
		}
	}
	fclose(file);
	free(filedata);
	return 0;
}
