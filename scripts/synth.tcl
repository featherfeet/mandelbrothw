set outputDir ./synth_output
file mkdir $outputDir
set_part xc7s25csga225-1
read_verilog [ glob ./rtl/*.v ]
read_xdc Cmod-S7-25-Master.xdc
synth_design -top top
write_checkpoint -force $outputDir/post_synth
opt_design
power_opt_design
place_design
phys_opt_design
write_checkpoint -force $outputDir/post_place
route_design
write_checkpoint -force $outputDir/post_route
write_bitstream -force $outputDir/design.bit
