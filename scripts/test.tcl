restart
log_wave -recursive *
add_force rst { 0 }
add_force clk { 0 0 } { 1 10 } -repeat_every 20
run 5
add_force rst { 1 }
run 15
add_force rst { 0 }
add_wave {{/top/clk}} 
current_wave_config {Untitled 1}
add_wave {{/top/mandelbrot/fpu_op}} 
current_wave_config {Untitled 1}
add_wave {{/top/mandelbrot/opa}} 
current_wave_config {Untitled 1}
add_wave {{/top/mandelbrot/opb}} 
current_wave_config {Untitled 1}
add_wave {{/top/mandelbrot/out}} 
current_wave_config {Untitled 1}
add_wave {{/top/mandelbrot/fpu_waiting_counter}} 
current_wave_config {Untitled 1}
add_wave {{/top/mandelbrot/state}} 
run 0.1s
