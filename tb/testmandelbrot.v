`timescale 1ns / 100ps

`define PIXEL_WIDTH_INT 100
`define PIXEL_HEIGHT_INT 100

module testmandelbrot();
    reg clk;
    reg rst;
    reg[9:0] pixel_x, pixel_y;
    wire[7:0] pixel_r, pixel_g, pixel_b;
    wire ready;
    initial
    begin
        clk <= 0;
        rst <= 0;
        pixel_x <= 0;
        pixel_y <= 0;
        #5 rst <= 1;
        #5 rst <= 0;
    end
    always
    begin
        #1 clk <= ~clk;
    end
    Mandelbrot mandelbrot(.rst(rst), .clk(clk), .pixel_x(pixel_x), .pixel_y(pixel_y), .pixel_r(pixel_r), .pixel_g(pixel_g), .pixel_b(pixel_b), .ready(ready));
    always @(posedge clk)
    begin
        if (ready)
        begin
            $display("%d,%d:%d,%d,%d", pixel_x, pixel_y, pixel_r, pixel_g, pixel_b);
            // Increment x and y.
             if (pixel_x < `PIXEL_WIDTH_INT)
                 pixel_x <= pixel_x + 1;
             else if (pixel_y < `PIXEL_HEIGHT_INT)
             begin
                 pixel_x <= 'b0;
                 pixel_y <= pixel_y + 1;
             end
             else
             begin
                 $finish;
             end
        end
    end
endmodule
