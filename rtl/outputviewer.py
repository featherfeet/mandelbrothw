#!/usr/bin/env python2

import sys
import cv2
import numpy as np

cv2.namedWindow("Mandelbrot. . . or not?", cv2.WINDOW_NORMAL)

key = -1

while key != ord('q'):
	filename = sys.argv[1]

	f = open(filename, 'r')

	img = np.zeros((101, 101, 3))

	for line in f:
		coordinates, color = line.split(':')
		x, y = coordinates.split(',')
		r, g, b = color.split(',')
		x = int(x)
		y = int(y)
		r = int(r)
		g = int(g)
		b = int(b)
		img[y, x, 0] = r
		img[y, x, 1] = g
		img[y, x, 2] = b

	f.close()

	cv2.imshow("Mandelbrot. . . or not?", img);
	key = cv2.waitKey(1)
cv2.destroyAllWindows()
