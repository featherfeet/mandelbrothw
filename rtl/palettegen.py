#!/usr/bin/env python3

import numpy as np
import colorsys

f = open("palette.hex", 'w')

for h in np.arange(0.0, 1.0, 1.0 / 1000.0):
	rgb = colorsys.hsv_to_rgb(h, 1.0, 1.0)
	rgb_8bit = list(rgb)
	rgb_8bit[0] *= 255
	rgb_8bit[1] *= 255
	rgb_8bit[2] *= 255
	rgb_8bit[0] = int(rgb_8bit[0])
	rgb_8bit[1] = int(rgb_8bit[1])
	rgb_8bit[2] = int(rgb_8bit[2])
	r = hex(rgb_8bit[0])[2:].zfill(2)
	g = hex(rgb_8bit[1])[2:].zfill(2)
	b = hex(rgb_8bit[2])[2:].zfill(2)
	f.write("{}{}{}\n".format(r, g, b))

f.close()
