`timescale 1ns / 100ps

`include "defines.v"

`define STATE_NORMALIZE_PIXEL_X    0
`define STATE_NORMALIZE_PIXEL_Y    1
`define STATE_DIVIDE_PIXEL_X       2
`define STATE_DIVIDE_PIXEL_Y       3
`define STATE_MULTIPLY_PIXEL_X     4
`define STATE_MULTIPLY_PIXEL_Y     5
`define STATE_SUBTRACT_PIXEL_X     6
`define STATE_SUBTRACT_PIXEL_Y     7
`define STATE_MANDELBROT           8
    `define SUBSTATE_START_MANDELBROT   0
    `define SUBSTATE_SQUARE_X           1
    `define SUBSTATE_SQUARE_Y           2
    `define SUBSTATE_X2_PLUS_Y2         3
    `define SUBSTATE_COMPARE_X2_Y2_4    4
    `define SUBSTATE_X2_MINUS_Y2        5
    `define SUBSTATE_XTEMP_ADD_PIXEL_X  6
    `define SUBSTATE_DOUBLE_X           7
    `define SUBSTATE_X_MULT_Y           8
    `define SUBSTATE_ADD_PIXEL_Y        9

`define FPU_OP_ADD 0
`define FPU_OP_SUB 1
`define FPU_OP_MUL 2
`define FPU_OP_DIV 3
`define FPU_OP_INT_TO_FLOAT 4

`define MAX_ITERATIONS 100

/*
Compute the color (r, g, b) of a point (x, y) in the Mandelbrot set. (x, y) is in pixel coordinates. `PIXEL_WIDTH and `PIXEL_HEIGHT must be defined for this to work.
*/
module Mandelbrot (
    input wire rst,
    input wire clk,
    input[31:0] pixel_x,
    input[31:0] pixel_y,
    output reg[7:0] pixel_r,
    output reg[7:0] pixel_g,
    output reg[7:0] pixel_b,
    output reg ready
);

    integer file;
    initial
    begin
        file = $fopen("mandelbrot.log", "w");
    end

    reg[2:0] fpu_op;
    reg[31:0] opa, opb;
    wire[31:0] out;
    fpu FPU(.clk(clk), .rmode('b0), .fpu_op(fpu_op), .opa(opa), .opb(opb), .out(out));
    reg[7:0] fpu_waiting_counter;

    reg[3:0] state;           // State of the main state machine.
    reg[31:0] pixel_x_float;  // The x-coordinate of the point in the Mandelbrot set currently being computed. 32-bit IEEE-754 float.
    reg[31:0] pixel_y_float;  // The y-coordinate of the point in the Mandelbrot set currently being computed. 32-bit IEEE-754 float.

    wire altb, blta, aeqb;
    fcmp FCMP(.opa(opa), .opb(opb), .altb(altb), .blta(blta), .aeqb(aeqb));

    reg[4:0] mandelbrot_state; // State of the sub-state-machine that does the math that determines the color of each point in the Mandelbrot set.
    reg[31:0] x;               // Variable used by the Mandelbrot escape-time algorithm to determine the pixel color.
    reg[31:0] y;               // Variable used by the Mandelbrot escape-time algorithm to determine the pixel color.
    reg[9:0] iteration;        // Variable used by the Mandelbrot escape-time algorithm to determine the pixel color.
    reg[31:0] x_squared;
    reg[31:0] y_squared;
    reg[31:0] x2_plus_y2;
    reg[31:0] xtemp;
    reg[31:0] x_times_2;
    reg[23:0] color;
    reg[2:0] color_waiting_counter;
    reg[23:0] palette[0:999];
    initial
    begin
        $readmemh("palette.hex", palette);
    end

    // State machine.
    always @(posedge clk)
    begin
        if (rst)
        begin
            opa <= 0;
            opb <= 0;
            fpu_op <= 0;
            fpu_waiting_counter <= 0;
            state <= 0;
            pixel_x_float <= 0;
            pixel_y_float <= 0;
            mandelbrot_state <= 0;
            x <= 0;
            y <= 0;
            iteration <= 0;
            x_times_2 <= 0;
            xtemp <= 0;
            color <= 0;
            color_waiting_counter <= 0;
            pixel_r <= 0;
            pixel_g <= 0;
            pixel_b <= 0;
        end
        case (state)
            // The following states (STATE_NORMALIZE_PIXEL_X through STATE_SUBTRACT_PIXEL_Y) do the following two calculations to convert the integer pixel coordinates to Mandelbrot imaginary coordinates:
            // pixel_x_float = -2.5 + (pixel_x / PIXEL_WIDTH) * 3.5
            // pixel_y_float = -1.0 + (pixel_y / PIXEL_HEIGHT) * 2.0
            `STATE_NORMALIZE_PIXEL_X:
            begin
                ready <= 0;
                fpu_op <= `FPU_OP_INT_TO_FLOAT;
                opa <= pixel_x;
                opb <= 0;
                fpu_waiting_counter <= fpu_waiting_counter + 1;
                if (fpu_waiting_counter == 6) // WAS 5
                begin
                    //$display("STATE_NORMALIZE_PIXEL_X: (%h, %h)", pixel_x_float, pixel_y_float);
                    state <= state + 1;
                    pixel_x_float <= out;
                    fpu_waiting_counter <= 0;
                end
            end
            `STATE_NORMALIZE_PIXEL_Y:
            begin
                fpu_op <= `FPU_OP_INT_TO_FLOAT;
                opa <= pixel_y;
                opb <= 0;
                fpu_waiting_counter <= fpu_waiting_counter + 1;
                if (fpu_waiting_counter == 5)
                begin
                    //$display("STATE_NORMALIZE_PIXEL_Y: (%h, %h)", pixel_x_float, pixel_y_float);
                    state <= state + 1;
                    pixel_y_float <= out;
                    fpu_waiting_counter <= 0;
                end
            end
            `STATE_DIVIDE_PIXEL_X:
            begin
                fpu_op <= `FPU_OP_DIV;
                opa <= pixel_x_float;
                opb <= `PIXEL_WIDTH;
                fpu_waiting_counter <= fpu_waiting_counter + 1;
                if (fpu_waiting_counter == 5)
                begin
                    //$display("STATE_DIVIDE_PIXEL_X: (%h, %h)", pixel_x_float, pixel_y_float);
                    state <= state + 1;
                    pixel_x_float <= out;
                    fpu_waiting_counter <= 0;
                end
            end
            `STATE_DIVIDE_PIXEL_Y:
            begin
                fpu_op <= `FPU_OP_DIV;
                opa <= pixel_y_float;
                opb <= `PIXEL_HEIGHT;
                fpu_waiting_counter <= fpu_waiting_counter + 1;
                if (fpu_waiting_counter == 5)
                begin
                    //$display("STATE_DIVIDE_PIXEL_Y: (%h, %h)", pixel_x_float, pixel_y_float);
                    state <= state + 1;
                    pixel_y_float <= out;
                    fpu_waiting_counter <= 0;
                end
            end
            `STATE_MULTIPLY_PIXEL_X:
            begin
                fpu_op <= `FPU_OP_MUL;
                opa <= pixel_x_float;
                opb <= 32'h40600000; // 3.5 in IEEE-754 format.
                fpu_waiting_counter <= fpu_waiting_counter + 1;
                if (fpu_waiting_counter == 5)
                begin
                    //$display("STATE_MULTIPLY_PIXEL_X: (%h, %h)", pixel_x_float, pixel_y_float);
                    state <= state + 1;
                    pixel_x_float <= out;
                    fpu_waiting_counter <= 0;
                end
            end
            `STATE_MULTIPLY_PIXEL_Y:
            begin
                fpu_op <= `FPU_OP_MUL;
                opa <= pixel_y_float;
                opb <= 32'h40000000; // 2.0 in IEEE-754 format.
                fpu_waiting_counter <= fpu_waiting_counter + 1;
                if (fpu_waiting_counter == 5)
                begin
                    //$display("STATE_MULTIPLY_PIXEL_Y: (%h, %h)", pixel_x_float, pixel_y_float);
                    state <= state + 1;
                    pixel_y_float <= out;
                    fpu_waiting_counter <= 0;
                end
            end
            `STATE_SUBTRACT_PIXEL_X:
            begin
                fpu_op <= `FPU_OP_SUB;
                opa <= pixel_x_float;
                opb <= 32'h40200000; // 2.5 in IEEE-754 format.
                fpu_waiting_counter <= fpu_waiting_counter + 1;
                if (fpu_waiting_counter == 5)
                begin
                    //$display("STATE_SUBTRACT_PIXEL_X: (%h, %h)", pixel_x_float, pixel_y_float);
                    state <= state + 1;
                    pixel_x_float <= out;
                    fpu_waiting_counter <= 0;
                end
            end
            `STATE_SUBTRACT_PIXEL_Y:
            begin
                fpu_op <= `FPU_OP_SUB;
                opa <= pixel_y_float;
                opb <= 32'h3f800000; // 1.0 in IEEE-754 format.
                fpu_waiting_counter <= fpu_waiting_counter + 1;
                if (fpu_waiting_counter == 5)
                begin
                    state <= `STATE_MANDELBROT;
                    pixel_y_float <= out;
                    fpu_waiting_counter <= 0;
                end
            end
            // This state does the escape-time algorithm to compute the color of point (pixel_x_float, pixel_y_float) in the Mandelbrot set.
            `STATE_MANDELBROT:
            begin
                // Sub-state-machine that does the escape-time algorithm.
                case (mandelbrot_state)
                    `SUBSTATE_START_MANDELBROT:
                    begin
                        x <= 0;
                        y <= 0;
                        iteration <= 0;
                        mandelbrot_state <= `SUBSTATE_SQUARE_X;
                    end
                    `SUBSTATE_SQUARE_X:
                    begin
                        opa <= x;
                        opb <= x;
			fpu_op <= `FPU_OP_MUL;
                        fpu_waiting_counter <= fpu_waiting_counter + 1;
                        if (fpu_waiting_counter == 5)
                        begin
                            mandelbrot_state <= `SUBSTATE_SQUARE_Y;
                            x_squared <= out;
                            fpu_waiting_counter <= 0;
                        end
                    end
                    `SUBSTATE_SQUARE_Y:
                    begin
                        opa <= y;
                        opb <= y;
			fpu_op <= `FPU_OP_MUL;
                        fpu_waiting_counter <= fpu_waiting_counter + 1;
                        if (fpu_waiting_counter == 5)
                        begin
                            mandelbrot_state <= `SUBSTATE_X2_PLUS_Y2;
                            y_squared <= out;
                            fpu_waiting_counter <= 0;
                        end
                    end
                    `SUBSTATE_X2_PLUS_Y2:
                    begin
                        opa <= x_squared;
                        opb <= y_squared;
			fpu_op <= `FPU_OP_ADD;
                        fpu_waiting_counter <= fpu_waiting_counter + 1;
                        if (fpu_waiting_counter == 5)
                        begin
                            mandelbrot_state <= `SUBSTATE_COMPARE_X2_Y2_4;
                            x2_plus_y2 <= out;
                            fpu_waiting_counter <= 0;
                        end
                    end
                    `SUBSTATE_COMPARE_X2_Y2_4:
                    begin
                        mandelbrot_state <= mandelbrot_state + 1;
                        opa <= x2_plus_y2;
                        opb <= 32'h40800000; // 4.0 in IEE-754
                        iteration <= iteration + 1;
			if ((altb == 1 || aeqb == 1) && iteration < `MAX_ITERATIONS - 2)
                            mandelbrot_state <= `SUBSTATE_X2_MINUS_Y2;
                        else
                        begin
                            // Lookup color.
                            if (iteration >= `MAX_ITERATIONS - 1)
                            begin
                                pixel_r <= 0;
                                pixel_g <= 0;
                                pixel_b <= 0;
                                state <= `STATE_NORMALIZE_PIXEL_X;
                                mandelbrot_state <= `SUBSTATE_START_MANDELBROT;
                                color_waiting_counter <= 0;
                                ready <= 1;
                            end
                            else
                            begin
                                color <= palette[iteration];
                                color_waiting_counter <= color_waiting_counter + 1;
                                if (color_waiting_counter == 2)
                                begin
                                    pixel_r <= color[23:16];
                                    pixel_g <= color[15:8];
                                    pixel_b <= color[7:0];
                                    state <= `STATE_NORMALIZE_PIXEL_X;
                                    mandelbrot_state <= `SUBSTATE_START_MANDELBROT;
                                    color_waiting_counter <= 0;
                                    ready <= 1;
                                end
                            end
                        end
                    end
                    `SUBSTATE_X2_MINUS_Y2:
                    begin
                        opa <= x_squared;
                        opb <= y_squared;
                        fpu_op <= `FPU_OP_SUB;
                        fpu_waiting_counter <= fpu_waiting_counter + 1;
                        if (fpu_waiting_counter == 5)
                        begin
                            mandelbrot_state <= `SUBSTATE_XTEMP_ADD_PIXEL_X;
                            xtemp <= out;
                            fpu_waiting_counter <= 0;
                        end
                    end
                    `SUBSTATE_XTEMP_ADD_PIXEL_X:
                    begin
                        opa <= xtemp;
                        opb <= pixel_x_float;
                        fpu_op <= `FPU_OP_ADD;
                        fpu_waiting_counter <= fpu_waiting_counter + 1;
                        if (fpu_waiting_counter == 5)
                        begin
                            mandelbrot_state <= `SUBSTATE_DOUBLE_X;
                            xtemp <= out;
                            fpu_waiting_counter <= 0;
                        end
                    end
                    `SUBSTATE_DOUBLE_X:
                    begin
                        opa <= x;
                        opb <= 32'h40000000; // 2.0 in IEEE-754 format.
                        fpu_op <= `FPU_OP_MUL;
                        fpu_waiting_counter <= fpu_waiting_counter + 1;
                        if (fpu_waiting_counter == 5)
                        begin
                            mandelbrot_state <= `SUBSTATE_X_MULT_Y;
                            x_times_2 <= out;
                            fpu_waiting_counter <= 0;
                        end
                    end
                    `SUBSTATE_X_MULT_Y:
                    begin
                        opa <= x_times_2;
                        opb <= y;
                        fpu_op <= `FPU_OP_MUL;
                        fpu_waiting_counter <= fpu_waiting_counter + 1;
                        if (fpu_waiting_counter == 5)
                        begin
                            mandelbrot_state <= `SUBSTATE_ADD_PIXEL_Y;
                            y <= out;
                            fpu_waiting_counter <= 0;
                        end
                    end
                    `SUBSTATE_ADD_PIXEL_Y:
                    begin
                        opa <= y;
                        opb <= pixel_y_float;
                        fpu_op <= `FPU_OP_ADD;
                        fpu_waiting_counter <= fpu_waiting_counter + 1;
                        if (fpu_waiting_counter == 5)
                        begin
                            mandelbrot_state <= `SUBSTATE_SQUARE_X;
                            y <= out;
                            x <= xtemp;
                            fpu_waiting_counter <= 0;
                        end
                    end
                endcase
            end
        endcase
    end

    always @(posedge ready)
    begin
        $fdisplay(file, "%h,%h,%t", pixel_x_float, pixel_y_float, $time());
        $fflush(file);
    end
endmodule
