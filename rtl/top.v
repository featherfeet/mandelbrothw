`timescale 1ns / 100ps

`include "defines.v"

module top (
    input wire rst,
    input wire clk,
    output wire uart_tx
);

    // Initialize a module (or modules) that computes the color of a given location in the mandelbrot set.
    reg[31:0] pixel_x; // Integer x-coordinate.
    reg[31:0] pixel_y; // Integer y-coordinate.
    wire[7:0] pixel_r, pixel_g, pixel_b;  // Integer red, green, and blue channels.
    wire ready;
    Mandelbrot mandelbrot(.rst(rst), .clk(clk), .pixel_x(pixel_x), .pixel_y(pixel_y), .pixel_r(pixel_r), .pixel_g(pixel_g), .pixel_b(pixel_b), .ready(ready));

    // Initialize registers and state machine.
    always @(posedge rst)
    begin
        pixel_x <= 'b0;
        pixel_y <= 'b0;
    end


    // Open a file to store the colors.
    integer file;
    initial
    begin
        file = $fopen("mandelbrot_output.txt", "w");
    end

    // Loop through coordinates.
    always @(posedge clk)
    begin
        if (ready)
        begin
             // Print color value.
             $fdisplay(file, "%d,%d:%d,%d,%d", pixel_x, pixel_y, pixel_r, pixel_g, pixel_b);
             $fflush(file);
             // Increment x and y.
             if (pixel_x < `PIXEL_WIDTH_INT)
                 pixel_x <= pixel_x + 1;
             else if (pixel_y < `PIXEL_HEIGHT_INT)
             begin
                 pixel_x <= 'b0;
                 pixel_y <= pixel_y + 1;
             end
             else
             begin
                 $finish;
             end
        end
    end
endmodule
